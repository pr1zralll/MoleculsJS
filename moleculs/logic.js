var ctx, w, h, objects = [];
$(document).ready(function () {
    init();
    setInterval(update, 15);

});

function init() {
    var canvas = document.getElementById("main");
    w = $(window).width();
    h = $(window).height();
    canvas.width = w;
    canvas.height = h;
    ctx = canvas.getContext("2d");
    ctx.font = "20px Arial";
    ctx.fillStyle = "#7caeff";
    canvas.addEventListener("click", function (e) {
        var rect = canvas.getBoundingClientRect();
        var x = e.clientX - rect.left;
        var y = e.clientY - rect.top;
        click(x, y);
    });
    for (i = 0; i < 1000; i++) {
        addBall(w / 2, h / 2);
    }
}

function click(x, y) {
    addBall(x, y);
}

function addBall(x, y) {
    objects.push(new Ball(x, y));
}

function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}
class Ball {
    constructor(x, y) {
        this.x = x;
        this.y = y;
        this.speed = getRandomArbitrary(4, 15);
        this.r = getRandomArbitrary(0, 3);
        this.dx = getRandomArbitrary(-1, 1);
        this.dy = getRandomArbitrary(-1, 1);
    }
    draw() {
        ctx.beginPath();
        ctx.arc(this.x, this.y, this.r, 0, 2 * Math.PI);
        ctx.fill();
    }
    update() {

        //border
        if (this.x + this.r > w)
            this.dx = -Math.abs(this.dx);
        if (this.x - this.r < 0)
            this.dx = Math.abs(this.dx);
        if (this.y + this.r > h)
            this.dy = -Math.abs(this.dy);
        if (this.y - this.r < 0)
            this.dy = Math.abs(this.dy);
        //move
        this.x += this.dx * this.speed;
        this.y += this.dy * this.speed;
        if (this.speed > 0) this.speed -= 0.1;
        //  else this.speed = 0;
    }
}

function update() {
    //clear
    ctx.clearRect(0, 0, w, h);

    //check collision
    check();
    //draw objects
    objects.forEach(function (item, index, array) {
        item.update();
        item.draw();
    });

    //fps
    //  ctx.fillText("fps: " + getFps(), 0, 30);
}

function check() {
    objects.forEach(function (item1, index1, array1) {
        objects.forEach(function (item2, index2, array2) {
            if (index1 != index2) {
                if (Math.sqrt(Math.pow((item1.x - item2.x), 2) + Math.pow((item1.y - item2.y), 2)) <= item1.r + item2.r) {
                    var tempX = item1.dx;
                    var tempY = item1.dy;
                    item1.dx = item2.dx;
                    item1.dy = item2.dy;
                    item2.dx = tempX;
                    item2.dy = tempY;
                    item2.update();
                    item1.update();
                }
            }
        });
    });
}

var lastLoop = new Date;

function getFps() {
    var thisLoop = new Date;
    var fps = 1000 / (thisLoop - lastLoop);
    lastLoop = thisLoop;
    return Math.round(fps);
}
